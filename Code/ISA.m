function [T, p, rho, mu, a] = ISA(h)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    ISA conditions
% 
% Gives thermodynamical values of the air for a given height.
%
% Input: 
%       - Height [m]
%
% Output:         
%       - T:   Temperature    [K]
%       - p:   Pressure       [Pa]
%       - rho: Density        [kg/m3]
%       - mu:  Viscosity      [kg/m/s]
%       - a:   Speed of sound [m/s]
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018


%% ISA
M   = 0.0289644;
g   = 9.80665;
R_u = 8.31447;
L   = 0.0065;
p_0 = 101325;
T_ref = 288.15;

Pressure_Factor = g * M / R_u / L;

T = T_ref * (1.0 - L * h / T_ref);
p = p_0   * (1.0 - L * h / T_ref)^Pressure_Factor;

rho = p * M / R_u / T; % Air density [kg/m3]

%% Sutherland's law
S_sth = 110.4;
C1    = 1.458 * 10^-6;
mu    = C1 * T^(3.0 / 2.0) / (T + S_sth);

%% Speed of sound
gamma = 1.4;

a = sqrt(gamma * p / rho); % [m/s]

end