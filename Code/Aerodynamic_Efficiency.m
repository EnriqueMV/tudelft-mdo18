function E = Aerodynamic_Efficiency(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    Aerodynamic Efficiency
% 
% Provides the Aerodyanmic Efficiency for Design_Point state
%
% Input: 
%       - Design_Point
%
% Output:         
%       - E: Aerodynamic efficiency
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

% Rescale the drag coefficient
CD_AW = Design_Point.Aerodynamics.CD_AW0 * Design_Point.Previous_Design.Geometry.S / Design_Point.Geometry.S;
CL_W  = Design_Point.Aerodynamics.CLwing;
CD_W  = Design_Point.Aerodynamics.CDwing;

% Compute aerodynamic efficiency
E = CL_W / (CD_AW + CD_W);

end