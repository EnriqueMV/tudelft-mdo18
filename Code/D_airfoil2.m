function [Xtu,Xtl,C,Thu,Thl,Cm] = D_airfoil2(Au,Al,X)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                      Airfoil Shape Toolbox
% 
% Transforms input Bernstein parameters (shape + class function method) to
% complete Airfoil coordinates 
%
% Input:
%      (Upper Bernstein coeff., Lower Bernstein coeff., X-coordinates)
%
%
% Output: 
%      [Upper [x,y(x)],         Lower [x,y(x)],         Class f. y(x) ,...
%       Upper thickness(x), Lower thickness(x), Camber(x)]
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

%% Create auxiliary variables
x = X(:,1);

N1 = 0.5;   % Class function N1
N2 = 1;     % Class function N2

zeta_u = 0.000;   % Upper surface TE gap
zeta_l = -0.000;  % Lower surface TE gap


nu = length(Au)-1;
nl = length(Al)-1;

%% Initialization
C  = zeros(1, length(x));
Su = zeros(1, length(x));
Sl = zeros(1, length(x));

Yu = zeros(1, length(x));
Yl = zeros(1, length(x));

Thu = zeros(1, length(x));
Thl = zeros(1, length(x));
Cm  = zeros(1, length(x));

%% Evaluate required functions for each X-coordiante
for i = 1:length(x)
    
    % Class Function for x(i):
    C(i) = (x(i)^N1) * (1.0 - x(i))^N2;
    
    % Shape Functions for upper and lower surface at x(i)
    
    Su(i) = 0;  % Shape function initially zero
    for j = 0:nu
        Krnu = factorial(nu) / (factorial(j) * factorial(nu-j));
        Su(i) = Su(i) + Au(j+1) * Krnu * (1-x(i))^(nu-j) * x(i)^(j);
    end
    
    Sl(i) = 0;  % Shape function initially zero
    
    for k = 0:nl        
        Krnl = factorial(nl) / (factorial(k) * factorial(nl-k));
        Sl(i) = Sl(i) + Al(k+1) * Krnl * (1.0 - x(i))^(nu-k) * x(i)^(k);
    end
    
    % Upper and lower surface coordinates at x(i)
    Yu(i) = C(i) * Su(i) + x(i) * zeta_u;
    Yl(i) = C(i) * Sl(i) + x(i) * zeta_l;
    
    Thu(i) = C(i) * (Su(i) - Sl(i)) / 2.0;    % Calculate lower thickness  !TE thickness ignored!
    Thl(i) = C(i) * (Sl(i) - Su(i)) / 2.0;    % Calculate upper thickness  !TE thickness ignored!
    Cm(i)  = C(i) * (Su(i) + Sl(i)) / 2.0;     % Calculate camber           !TE thickness ignored!
end

Yust = Yu';
Ylst = Yl';

%% Assemble airfoil coordinates matrix
Xtu = [x  Yust];
Xtl = [x  Ylst];