Multidisciplinary Optimization 2017/18
======================================
Enrique Millan Valbuena
462 426 8

An effort has been made to make all the variable names self-explicative.
For further Q&A, please email:

enrique.millanvalbuena@gmail.com


What Is This?
-------------

The following code corresponds to the MDO assignment for the academic year 2017/18.

A multidisciplinary framework MDF has been built and set-up in MatLab. Within this environment it shall minimize the amount of fuel a given aircraft requires for a design range and payload. Only the wing of the airplane will be modelled as a geometrical figure: the planform is built by joining two trapezoidal elements and the thickness given as an interpolation between three airfoil sections at the root, kink and tip. It will be seen by four different disciplines: Loads, Aerodynamic Performance, Structural Analysis and finally the Breguet Fraction Method for Performance evaluations.

How To: Previous Design
-----------------------

Once the user has selected the aircraft it wants to optimize, it shall load all the known data into the dedicated function:

	Previous_Design

This function will generate a MatLab file called:

	Initial_Design.mat

This file will be used by the optimizer to compute the variations with respect to the reference aircraft.

How To: Optimization
--------------------

To run the optimization procedure, run:

	Optimizer.m

Additionally, if any bound or optimization option wants to be set, it should be done in this file.

Once the optimization is finished, run:

	Constraints_Calculation.m

if you need the value of the constraints for each iteration point.
