function FW = Fuel_Breguet(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    Breguet Fraction Method
%
% Input: 
%       - Design_Point
%
% Output:         
%       - FW: Fuel Weight for Design_Point Range and E
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

% Fraction method
alpha = 0.938; 

% Airplane constants
B = Design_Point.Flight.V_cruise / (Design_Point.Flight.CT);
E = Design_Point.Aerodynamics.E;
R = Design_Point.Flight.R;

% Breguet equation
FW = (alpha * exp(R / B / E) - 1.0) * (Design_Point.M_AW + Design_Point.Wing_Weight);

end