function [CST_Coefficients, t_c_max] = Bernstein_Airfoils(N_coef)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                 Airfoil CST fitting
% 
% Provides the CST coefficients and the thickness to chord
% ratio for a given airfoil.
%
% Input: 
%       - N_coef: number of coefficients per surface
%
% Output:         
%       - CST_Coefficients: [1 : N_coef, N_coef + 1 : end] 
%                            Upper part  Lower part
%       - t_c_max: thickness-to-chord ratio
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

whitcomb = importdata('Whitcomb.dat');

n = (length(whitcomb(:,1))-1) / 2;

%% Upper foil (goes from 1 to 0, needs change)
x_upper_aux = whitcomb(1:37,1);
y_upper_aux = whitcomb(1:37,2);

x_upper = whitcomb(1:37,1);
y_upper = whitcomb(1:37,2);

% Swap directions
for i = 0:n
    x_upper(i+1) =  x_upper_aux((n+1)-i);
    y_upper(i+1) =  y_upper_aux((n+1)-i);
end

%% Lower foil
x_lower_aux = whitcomb(37:end,1);
y_lower_aux = whitcomb(37:end,2);

x_lower = whitcomb(37:end,1);
y_lower = whitcomb(37:end,1);

% Swap directions
for i = 0:n
    x_lower(i+1) =  x_lower_aux((n+1)-i);
    y_lower(i+1) =  y_lower_aux((n+1)-i);
end

%% Compute thickness
y_difference = y_upper - y_lower;

t_c_max = max(100 * y_difference);

%fprintf('Maximum thickness : %f', max(100 * y_difference))

%% Berstein polynomials
% N_coef = 10;
% cont = cont + 1;
Au0 = ones(1, N_coef);
Al0 = ones(1, N_coef) .* (-1.0);

DesignVec0 = Au0;

%Bounds
lb = -10.0 .* ones(length(Au0),1);
ub =  10.0  .* ones(length(Au0),1);

%options = optimoptions('fmincon','MaxIterations', 10^6);

%% Upper coefficients
% Define anonymous function to evaluate error
f = @(DesignVec) Error_Function( DesignVec, x_upper, y_upper );

%A�adimos reestricciones
[DesignVec, ~] = fmincon(f, DesignVec0,[],[],[],[],lb,ub,[],[]);

Upper_Coefficients = reshape(DesignVec, [1, N_coef]);

%% Lower coefficients
% Define anonymous function to evaluate error
f = @(DesignVec) Error_Function( DesignVec, x_lower, y_lower );

%A�adimos reestricciones
DesignVec0 = Al0;
[DesignVec, ~] = fmincon(f, DesignVec0,[],[],[],[],lb,ub,[],[]);

Lower_Coefficients = reshape(DesignVec, [1, N_coef]);

%% Build coefficients row
CST_Coefficients = [Upper_Coefficients Lower_Coefficients];


