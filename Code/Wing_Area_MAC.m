function [S_Half_Wing, MAC] = Wing_Area_MAC(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                   Wing area and MAC computations
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

% Extract information from Design_Point
Chords = Design_Point.Geometry.Chords;

b   = Design_Point.Geometry.b;
b_k = Design_Point.Geometry.b_k;

% Trapezoidal elements surfaces
S1          =        b_k  * (Chords(1) + Chords(2)) / 2.0;
S2          = (b/2 - b_k) * (Chords(2) + Chords(3)) / 2.0;
S_Half_Wing = (S1 + S2);

% Mean aerodynamic chord (MAC)
Taper_Ratio_Kink = Chords(2) / Chords(1);
Taper_Ratio_Tip  = Chords(3) / Chords(2);

% Trapezoidal wing calculation
MAC_1 = (2.0 / 3.0) * Chords(1) * (1.0 + Taper_Ratio_Kink + Taper_Ratio_Kink^2) / (1.0 + Taper_Ratio_Kink);
MAC_2 = (2.0 / 3.0) * Chords(2) * (1.0 + Taper_Ratio_Tip  + Taper_Ratio_Tip^2 ) / (1.0 + Taper_Ratio_Tip );

% MAC computation
MAC  = (S1 * MAC_1 + S2 * MAC_2 ) / S_Half_Wing;

end