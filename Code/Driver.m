function [objective, c_ineq, c_eq] = Driver(x)

global Design_Point

%% MDA Driver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          MDA Driver
% 
% Manages the Gauss-Seidel iteration loop.
% Gives back a feasible solution for the coupled disciplines.
%
% Tool(s): EMWET, Q3D_Solver, Breguet
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018


%% Initialize Design Point
%
% Only used to size initial design;
%
% Design_Point = Previous_Design;
% %load Design_0.mat
%

%% Extract design vector
% %                           _____ _____ ___
% %x = [b Chord_r Taper_ratio Sweep Twist CST]
% 

N_CSTa = Design_Point.Geometry.N_CST / 3;

% Compute new physical values from design vector
Design_Point.Geometry.b           = x(1) .* Design_Point.Previous_Design.Geometry.b;           % [m]
Design_Point.Geometry.Chords(1)   = x(2) .* Design_Point.Previous_Design.Geometry.Chords(1);   % [m]
Design_Point.Geometry.Taper_Ratio = x(3) .* Design_Point.Previous_Design.Geometry.Taper_Ratio; % []
Design_Point.Geometry.Sweep(1)    = x(4) .* Design_Point.Previous_Design.Geometry.Sweep(1);    % [deg]
Design_Point.Geometry.Sweep(2)    = x(5) .* Design_Point.Previous_Design.Geometry.Sweep(2);    % [deg]
Design_Point.Geometry.Twist(1)    = x(6) .* Design_Point.Previous_Design.Geometry.Twist(1);    % [deg]
Design_Point.Geometry.Twist(2)    = x(7) .* Design_Point.Previous_Design.Geometry.Twist(2);    % [deg]
Design_Point.Geometry.Twist(3)    = x(8) .* Design_Point.Previous_Design.Geometry.Twist(3);    % [deg]

% Extract CST increments
y = reshape(x(9:end), [N_CSTa, 3])';

Design_Point.Geometry.CST(1,:) = y(1,:) .* Design_Point.Previous_Design.Geometry.CST(1,:); % []
Design_Point.Geometry.CST(2,:) = y(2,:) .* Design_Point.Previous_Design.Geometry.CST(2,:); % []
Design_Point.Geometry.CST(3,:) = y(3,:) .* Design_Point.Previous_Design.Geometry.CST(3,:); % []

%% Update design derived variables

% CHORDS
% a) Kink
Design_Point.Geometry.Chords(2) = Design_Point.Geometry.Chords(1) + ...
                                  Design_Point.Geometry.b_k * (tand(Design_Point.Geometry.Sweep_Kink) - ...
                                                               tand(Design_Point.Geometry.Sweep(1)));
% b) Tip
Design_Point.Geometry.Chords(3) = Design_Point.Geometry.Chords(1) * Design_Point.Geometry.Taper_Ratio;

% Airfoil positions
[x_a, y_a, z_a] = Airfoil_Positions(Design_Point);

Design_Point.Geometry.Root.x = x_a(1);
Design_Point.Geometry.Kink.x = x_a(2);
Design_Point.Geometry.Tip.x  = x_a(3);

Design_Point.Geometry.Root.y = y_a(1);
Design_Point.Geometry.Kink.y = y_a(2);
Design_Point.Geometry.Tip.y  = y_a(3);

Design_Point.Geometry.Root.z = z_a(1);
Design_Point.Geometry.Kink.z = z_a(2);
Design_Point.Geometry.Tip.z  = z_a(3);

% Wing surface and MAC
[Design_Point.Geometry.S2, Design_Point.Geometry.MAC] = Wing_Area_MAC(Design_Point);
Design_Point.Geometry.S = 2.0 * Design_Point.Geometry.S2;

% Reynolds number
Design_Point.Flight.Re  = Design_Point.Geometry.MAC * Design_Point.Flight.V_cruise ...
                          * Design_Point.Flight.rho / Design_Point.Flight.mu;

% Non-lifting surfaces drag coefficient
% Design_Point.Aerodynamics.CD_AW0 = Design_Point.Aerodynamics.CD_AW0 * Design_Point.Previous_Design.S / Design_Point.Geometry.S;

%% Prepare iteration procedure

% Maximum iterations
Max_Iterations = Design_Point.MDA.Max_Iterations;

% Iteration variables
MDA.Ww = zeros(1, Max_Iterations+1);
MDA.E  = zeros(1, Max_Iterations+1);
MDA.FW = zeros(1, Max_Iterations+1);

% Error vectors
Error_MDA.FW = zeros(1, Max_Iterations+1);
Error_MDA.E  = zeros(1, Max_Iterations+1);
Error_MDA.Ww = zeros(1, Max_Iterations+1);

% Load initial state
MDA.Ww(1) = Design_Point.Wing_Weight;
MDA.E(1)  = Design_Point.Aerodynamics.E;
MDA.FW(1) = Design_Point.FW;

    %% Gauss-Seidel iteration
    for MDA_Iteration = 2:Max_Iterations
        
        %fprintf('\n++++++++++++++++++++++++++++++')
        %fprintf('\n Iteration: %i \n', MDA_Iteration-1)
        %fprintf('++++++++++++++++++++++++++++++')
        
        % -------------
        % Load analysis
        % -----------------------------------------------------------------
        cd Q3DSolver_Inviscid
            [Loads, Inviscid_Aerodynamics] = Q3D_Inviscid(Design_Point);
        cd ..
        % Update analysis
            Design_Point.Aerodynamics.Sizing.Loads    = Loads;
            Design_Point.Aerodynamics.Sizing.Analysis = Inviscid_Aerodynamics;
        % -----------------------------------------------------------------
        
        % -------------------
        % Structural Analysis
        % -----------------------------------------------------------------
        [MDA.Ww(MDA_Iteration), Design_Point.FuelTank.Volume]...
            = EMWET_Start(Design_Point, Loads);
        Design_Point.Wing_Weight = MDA.Ww(MDA_Iteration);
        %------------------------------------------------------------------
        
        % ----------------------
        % Aerodynamical Analysis
        % -----------------------------------------------------------------
        cd q3dsolver_viscid
            [Aerodynamic_Analysis] = Q3D_Viscid(Design_Point);
            
            % Update lift and drag
            Design_Point.Aerodynamics.CLwing = Aerodynamic_Analysis.CLwing;
            Design_Point.Aerodynamics.CDwing = Aerodynamic_Analysis.CDwing;
            
            % Update analysis
            Design_Point.Aerodynamics.Cruise_Analysis = Aerodynamic_Analysis;
            
        cd ..
        
            % Compute aerodynamic efficiency
            MDA.E(MDA_Iteration) = Aerodynamic_Efficiency(Design_Point);
        
            % Protection against NaN
            if isnan(MDA.E(MDA_Iteration))
                objective = NaN;
                fprintf('\n\n f(x) = NaN; The flow is reversed on the wing')
                return
            end
        
        Design_Point.Aerodynamics.E = MDA.E(MDA_Iteration);
        % -----------------------------------------------------------------
        
        % -----------
        % Performance
        % -----------------------------------------------------------------
        MDA.FW(MDA_Iteration) = Fuel_Breguet(Design_Point);
        
        % Update Fuel Weight in Design_Point
        Design_Point.FW = MDA.FW(MDA_Iteration);
        % -----------------------------------------------------------------
        
        save MDA_Current_Point MDA_Iteration Design_Point MDA Error_MDA
        
        % Update error vector
            Error_MDA.FW(MDA_Iteration) = abs(MDA.FW(MDA_Iteration) - MDA.FW(MDA_Iteration-1)) / MDA.FW(MDA_Iteration);
            Error_MDA.E(MDA_Iteration)  = abs(MDA.E(MDA_Iteration)  - MDA.E(MDA_Iteration-1))  / MDA.E(MDA_Iteration);
            Error_MDA.Ww(MDA_Iteration) = abs(MDA.Ww(MDA_Iteration) - MDA.Ww(MDA_Iteration-1)) / MDA.Ww(MDA_Iteration);
            
            %fprintf('\n Error_FW: %e,  Error_E: %e, Error_MDA.Ww: %e', ...
            %    Error_MDA.FW(MDA_Iteration), Error_MDA.E(MDA_Iteration), Error_MDA.Ww(MDA_Iteration))
            
            % In case it is a function evaluation at design point
            if (Error_MDA.FW(MDA_Iteration) == 0.0 && ...
                Error_MDA.E(MDA_Iteration)  == 0.0 && ...
                Error_MDA.Ww(MDA_Iteration) == 0.0       )
            
                break
                
            end %: In case error is exactly zero
            
            % In case it is an evaluation different from design point
            if (Error_MDA.FW(MDA_Iteration) < Design_Point.tolerances.MDA.FW && ...
                Error_MDA.E(MDA_Iteration)  < Design_Point.tolerances.MDA.E  && ...
                Error_MDA.Ww(MDA_Iteration) < Design_Point.tolerances.MDA.Ww)
            
                % Extract values
                MDA.Ww = MDA.Ww(1:MDA_Iteration);
                MDA.E  = MDA.E(1:MDA_Iteration);
                MDA.FW = MDA.FW(1:MDA_Iteration);
                
                MDA.Converged_Iteration = MDA_Iteration;
            
                break
                
            end % if: MDA convergence check
            
    end % MDA_iterations
    
    % Protection against non-convergent MDA
    if (MDA_Iteration == Max_Iterations)
        fprintf('\n\n MDA did not converge in that number of iterations')
        objective = NaN;
        c_ineq = [NaN NaN];
        c_eq   = [];
        return
    end
    
%% Update design point

Design_Point.MTOW = Design_Point.M_AW + ...
                    Design_Point.FW   + ...
                    Design_Point.Wing_Weight;

% Save current design point in case solver crashes
save MDA_Converged_Solution Design_Point MDA

%% Objective function

objective = Design_Point.FW / Design_Point.Previous_Design.FW;

%% Constraints

% Fuel volume
rho_fuel    = Design_Point.FuelTank.Density;
f_tank      = Design_Point.FuelTank.f_tank;
Tank_Volume = Design_Point.FuelTank.Volume;
FW_0        = Design_Point.Previous_Design.FW;

c_FuelVolume  = (Design_Point.FW / rho_fuel     - f_tank * Tank_Volume)  / (FW_0 / rho_fuel);       % Consistency between design fuel volume and fuel tank volume

% Wing loading

MTOW   = Design_Point.MTOW;
MTOW_0 = Design_Point.Previous_Design.MTOW;

Wings_Area   = Design_Point.Geometry.S;
Wings_Area_0 = Design_Point.Previous_Design.Geometry.S;

c_WingLoad    = (MTOW / Wings_Area - MTOW_0 / Wings_Area_0) / (MTOW_0 / Wings_Area_0); % Take-off/landing performance can be achievied with the new wing

% Aspect ratio

c_AR = (Design_Point.Geometry.AR0 - Design_Point.Geometry.b^2 / Design_Point.Geometry.S) / Design_Point.Geometry.AR0;

c_ineq = [c_WingLoad c_FuelVolume, c_AR];
c_eq   = [];

%% Plot results
% figure
% subplot(311)
%     plot(MDA.Ww, '--o', 'LineWidth', 0.5)
%     
%     xlabel('i')
%     ylabel('Wing Weight')
%     grid
%     
%     title('Variables value during MDA iteration process')
%     
% subplot(312)
%     plot(MDA.E, '--o', 'LineWidth', 0.5)
%     
%     xlabel('i')
%     ylabel('MDA.E')
%     grid
%     
% subplot(313)
%     plot(MDA.FW, '--o', 'LineWidth', 0.5)
%     
%     xlabel('i')
%     ylabel('Fuel Weight')
%     grid
    
    
    
end
    