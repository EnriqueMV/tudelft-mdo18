%% Compute the constraints history

load Optimization_Problem_Results.mat

N = OUTPUT.iterations;

for i = 1:N
    fprintf('Iteration = %i', i)
    
    design_space = History.x(i,:);
    
    [objective(i), c_ineq, c_eq] = Driver(design_space);
    
    Constraints_Value.c_WingLoad(i)   = c_ineq(1);
    Constraints_Value.c_FuelVolume(i) = c_ineq(2);
    Constraints_Value.c_AR(i)         = c_ineq(3);
    
end

%% Check
% c_ineq = [c_WingLoad c_FuelVolume, c_AR];

figure
    subplot(4,1,1)
    plot(objective)
    ylabel('Objective function')
    
    subplot(4,1,2)
    plot(Constraints_Value.c_WingLoad)
    ylabel('Wing Load')
    
    subplot(4,1,3)
    plot(Constraints_Value.c_FuelVolume)
    ylabel('Objective function')
    
    subplot(4,1,4)
    plot(Constraints_Value.c_AR)
    ylabel('Objective function')
