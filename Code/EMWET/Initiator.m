function I = Initiator(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                 STRUCTURAL ANALYSIS - Initiator
% 
% Generates MatLab structure to write down xxx.init.
%
% Tool: Partially provided by TU Delft.
%
% Contains:
%   
%   - Wing geometrical information
%   - Power Plant (PP) information
%   - Wing Material information
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

%% Extract data from Design Point

b   = Design_Point.Geometry.b;
b_k = Design_Point.Geometry.b_k;

Chords = Design_Point.Geometry.Chords; % [m]
Sweep  = Design_Point.Geometry.Sweep;  % [deg]

Delta_Kink = Design_Point.Geometry.Sweep_Kink; % [deg]

% Chords
Chord_Root = Chords(1);
Chord_Kink = Chord_Root + b_k * (tand(Delta_Kink) - tand(Sweep(1)));
Chord_Tip  = Chords(3);

% Fuel tank (Fixed)
Fuel_tank_start = Design_Point.FuelTank.Start; % To avoid problems with at the root
Fuel_tank_end   = Design_Point.FuelTank.End;

% Airfoil positions
x_root = Design_Point.Geometry.Root.x;  % Root leading edge x-position
y_root = Design_Point.Geometry.Root.y;  % Root leading edge y-position
z_root = Design_Point.Geometry.Root.z;  % Root leading edge z-position

x_kink = Design_Point.Geometry.Kink.x;  % Kink leading edge x-position
y_kink = Design_Point.Geometry.Kink.y;  % Kink leading edge y-position
z_kink = Design_Point.Geometry.Kink.z;  % Kink leading edge z-position

x_tip = Design_Point.Geometry.Tip.x; % Tip leading edge x-position
y_tip = Design_Point.Geometry.Tip.y; % Tip leading edge y-position                                                    
z_tip = Design_Point.Geometry.Tip.z; % Tip leading edge z-position 

%% Design weights
% Weights
if Design_Point.Conditions.Design == 0
    MTOW = Design_Point.Previous_Design.MTOW;
    %fprintf('\n Loads MTOW: %f \n', MTOW)
else
    MTOW = Design_Point.M_AW + Design_Point.Wing_Weight + Design_Point.FW;
    %fprintf('\n Loads MTOW: %f \n', MTOW)
end

I.Weight.MTOW  =  MTOW;             % [kg]           
I.Weight.MZFW  =  Design_Point.MZFW; % [kg]    
I.n_max        =  Design_Point.n;   % []   MTOW > 22 680 kg

%% Wing Geometry
 
I.Wing(1).Area          =  Design_Point.Geometry.S;
I.Wing(1).Span          =  b;
I.Wing(1).SectionNumber =  3;
I.Wing(1).AirfoilNumber =  3;

I.Wing(1).AirfoilName =     {'AF1'      'AF2'      'AF3'};
I.Wing(1).AirfoilPosition = [  0    b_k / (b / 2.0)   1  ];


% --- AF1 (Root) ---

I.Wing(1).WingSection.Chord = Chord_Root;
I.Wing(1).WingSection.Xle   = x_root;
I.Wing(1).WingSection.Yle   = y_root;
I.Wing(1).WingSection.Zle   = z_root;

I.Wing(1).WingSection.FrontSparPosition = Design_Point.Spars.Root.Front;
I.Wing(1).WingSection.RearSparPosition  = Design_Point.Spars.Root.Rear;

% --- AF2 (Kink) ---

I.Wing(2).WingSection.Chord = Chord_Kink;
I.Wing(2).WingSection.Xle   = x_kink;
I.Wing(2).WingSection.Yle   = y_kink;
I.Wing(2).WingSection.Zle   = z_kink;

I.Wing(2).WingSection.FrontSparPosition = Design_Point.Spars.Kink.Front;
I.Wing(2).WingSection.RearSparPosition  = Design_Point.Spars.Kink.Rear;

% --- AF3 (Tip) ---

I.Wing(3).WingSection.Chord = Chord_Tip;
I.Wing(3).WingSection.Xle   = x_tip;
I.Wing(3).WingSection.Yle   = y_tip;
I.Wing(3).WingSection.Zle   = z_tip;

I.Wing(3).WingSection.FrontSparPosition = Design_Point.Spars.Tip.Front;
I.Wing(3).WingSection.RearSparPosition  = Design_Point.Spars.Tip.Rear;

%% Fuel tanks geometry

I.WingFuelTank.Ystart = Fuel_tank_start;
I.WingFuelTank.Yend   = Fuel_tank_end;

%% Power plant and landing gear and wing atructure

I.PP(1).WingEngineNumber = 0;  
I.PP(1).EnginePosition   = Design_Point.Engine.Position; % [m]
I.PP(1).EngineWeight     = Design_Point.Engine.Weight;   % [kg]


%% Material and Structure

I.Material.Wing.UpperPanel.E                 = 7.1e10;
I.Material.Wing.UpperPanel.rho               = 2800;
I.Material.Wing.UpperPanel.Sigma_tensile     = 2.95e8;
I.Material.Wing.UpperPanel.Sigma_compressive = 2.95e8;

I.Material.Wing.LowerPanel.E                 = 7.1e10;
I.Material.Wing.LowerPanel.rho               = 2800;
I.Material.Wing.LowerPanel.Sigma_tensile     = 2.95e8;
I.Material.Wing.LowerPanel.Sigma_compressive = 2.95e8;

I.Material.Wing.FrontSpar.E                 = 7.1e10;
I.Material.Wing.FrontSpar.rho               = 2800;
I.Material.Wing.FrontSpar.Sigma_tensile     = 4.8e8;
I.Material.Wing.FrontSpar.Sigma_compressive = 4.6e8;

I.Material.Wing.RearSpar.E                 = 7.1e10;
I.Material.Wing.RearSpar.rho               = 2800;
I.Material.Wing.RearSpar.Sigma_tensile     = 4.8e8;
I.Material.Wing.RearSpar.Sigma_compressive = 4.6e8;


I.Structure.Wing.UpperPanelEfficiency = 0.96;  
I.Structure.Wing.RibPitch             = 0.5;







