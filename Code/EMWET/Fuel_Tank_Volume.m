function [Tank_Volume] = Fuel_Tank_Volume(Design_Point)

%% Reading the full output
[position, chords, thickness] = read_EMWET;

%% Data extraction

b     = Design_Point.Geometry.b;
b_k   = Design_Point.Geometry.b_k;

%% Search for kink location
% We look up the index for the kink in the position vector

for i = 1:length(position)
    if position(i) > b_k / (b / 2)
        kink_index = i;
        break
    end
end

%% Search for fuel tank beginning and end location
% We look up the index for the start and end of the fuel tank in the position vector

for i = 1:length(position)
    if position(i) >= Design_Point.FuelTank.Start
        fuel_tank_start = i;
        break
    end
end

for i = 1:length(position)
    if position(i) >= Design_Point.FuelTank.End
        fuel_tank_end = i;
        break
    end
end

%% Fuel tank domain (x-direction)

% It has been assumed all spars are located in the same spot
Front_Spar_Location = chords .* Design_Point.Spars.Root.Front; % Front spar location
Rear_Spar_Location  = chords .* Design_Point.Spars.Root.Rear;  % Rear spar location

N = length(chords);

Domain_Start = zeros(1,N);
Domain_End   = zeros(1,N);

% Spanwise beginning and end of the fuel tank in the x-direction
for i = 1:N
    Domain_Start(i) = Front_Spar_Location(i) + thickness.left(i)  / 2.0;
    Domain_End  (i) = Rear_Spar_Location(i)  - thickness.right(i) / 2.0;
end

%% Upper and lower surface calculation (z-direction)

N_nodes = 100;
j       = linspace(N_nodes,0, N_nodes);

x = cos(j .* pi ./ N_nodes);
x = (1.0 + x) ./ 2.0;

% Airfoil curves

N_CSTa = numel(Design_Point.Geometry.CST(1,:));

[Xt_Upper_Root , Xt_Lower_Root , ~, ~, ~, ~] = D_airfoil2(Design_Point.Geometry.CST(1,1:N_CSTa/2)' , Design_Point.Geometry.CST(1, N_CSTa/2 + 1:N_CSTa)' , x'); % Root
[Xt_Upper_Kink , Xt_Lower_Kink , ~, ~, ~, ~] = D_airfoil2(Design_Point.Geometry.CST(2,1:N_CSTa/2)' , Design_Point.Geometry.CST(2, N_CSTa/2 + 1:N_CSTa)' , x'); % Kink
[Xt_Upper_End  , Xt_Lower_End  , ~, ~, ~, ~] = D_airfoil2(Design_Point.Geometry.CST(3,1:N_CSTa/2)' , Design_Point.Geometry.CST(3, N_CSTa/2 + 1:N_CSTa)' , x'); % Tip

% Scaling with chord length
Xt_Upper_Root = Xt_Upper_Root .* chords(fuel_tank_start);
Xt_Lower_Root = Xt_Lower_Root .* chords(fuel_tank_start);

Xt_Upper_Kink = Xt_Upper_Kink .* chords(kink_index);
Xt_Lower_Kink = Xt_Lower_Kink .* chords(kink_index);

Xt_Upper_End = Xt_Upper_End .* chords(fuel_tank_end);
Xt_Lower_End = Xt_Lower_End .* chords(fuel_tank_end);

%% Plotting
% figure
%     plot(Xt_Upper_Root(:,1) , Xt_Upper_Root(:,2))
%     hold on
%     plot(Xt_Lower_Root(:,1) , Xt_Lower_Root(:,2))
%     legend('Upper surface', 'Lower surface')
% 
%     axis equal
%% Fuel tank y(x) function at each station
% Note to self: the proper way to do things is interpolate all the airfoils
% between the root, kink and the end, and use this surfaces in order to
% integrate the areas. 

% As a first approach, the volume of a trapezoidal volume cut in two
% parallel planes will be used, where the top-most and bottom-most areas
% are given by the root, kink and end areas.

% Piecewise definition of the integration surface
% Technique: we exploit the MatLab possibility to build boolean vectors:
% e.g: Domain_Start(fuel_tank_start) < Xt_Upper_Root(:,1)

% Root
Y_Upper_Root = Xt_Upper_Root(:,2)               .* (Domain_Start(fuel_tank_start) < Xt_Upper_Root(:,1)) .* (Xt_Upper_Root(:,1) < Domain_End(fuel_tank_start)) - ...
               thickness.upper(fuel_tank_start) .* (Domain_Start(fuel_tank_start) < Xt_Upper_Root(:,1)) .* (Xt_Upper_Root(:,1) < Domain_End(fuel_tank_start));
           
Y_Lower_Root = Xt_Lower_Root(:,2)               .* (Domain_Start(fuel_tank_start) < Xt_Upper_Root(:,1)) .* (Xt_Upper_Root(:,1) < Domain_End(fuel_tank_start)) + ...
               thickness.lower(fuel_tank_start) .* (Domain_Start(fuel_tank_start) < Xt_Upper_Root(:,1)) .* (Xt_Upper_Root(:,1) < Domain_End(fuel_tank_start));

% Kink
Y_Upper_Kink = Xt_Upper_Kink(:,2)          .* (Domain_Start(kink_index) < Xt_Upper_Kink(:,1)) .* (Xt_Upper_Kink(:,1) < Domain_End(kink_index)) - ... 
               thickness.upper(kink_index) .* (Domain_Start(kink_index) < Xt_Upper_Kink(:,1)) .* (Xt_Upper_Kink(:,1) < Domain_End(kink_index));
           
Y_Lower_Kink = Xt_Lower_Kink(:,2)          .* (Domain_Start(kink_index) < Xt_Upper_Kink(:,1)) .* (Xt_Upper_Kink(:,1) < Domain_End(kink_index)) + ...
               thickness.lower(kink_index) .* (Domain_Start(kink_index) < Xt_Upper_Kink(:,1)) .* (Xt_Upper_Kink(:,1) < Domain_End(kink_index));

% Tip
Y_Upper_End = Xt_Upper_End(:,2)              .* (Domain_Start(fuel_tank_end) < Xt_Upper_End(:,1)) .* (Xt_Upper_End(:,1) < Domain_End(fuel_tank_end)) - ...
              thickness.upper(fuel_tank_end) .* (Domain_Start(fuel_tank_end) < Xt_Upper_End(:,1)) .* (Xt_Upper_End(:,1) < Domain_End(fuel_tank_end));
          
Y_Lower_End = Xt_Lower_End(:,2)              .* (Domain_Start(fuel_tank_end) < Xt_Upper_End(:,1)) .* (Xt_Upper_End(:,1) < Domain_End(fuel_tank_end)) + ...
              thickness.lower(fuel_tank_end) .* (Domain_Start(fuel_tank_end) < Xt_Upper_End(:,1)) .* (Xt_Upper_End(:,1) < Domain_End(fuel_tank_end));

%% Plotting
% figure
%     plot(Xt_Upper_Root(:,1) , Xt_Upper_Root(:,2))
%     hold on
%     plot(Xt_Lower_Root(:,1) , Xt_Lower_Root(:,2))
%     plot(Xt_Upper_Root(:,1) , Y_Upper_Root, '--', 'LineWidth', 1.5)
%     plot(Xt_Lower_Root(:,1) , Y_Lower_Root, '--', 'LineWidth', 1.5)          
%     legend('US (Root)', 'LS (Root)', 'US-Tank (Root)', 'LS-Tank (Root)')
%     
%     grid minor
%     axis equal
% 
% figure
%     plot(Xt_Upper_Kink(:,1) , Xt_Upper_Kink(:,2))
%     hold on
%     plot(Xt_Lower_Kink(:,1) , Xt_Lower_Kink(:,2))
%     plot(Xt_Upper_Kink(:,1) , Y_Upper_Kink, '--', 'LineWidth', 1.5)
%     plot(Xt_Lower_Kink(:,1) , Y_Lower_Kink, '--', 'LineWidth', 1.5')          
%     legend('US (Kink)', 'LS (Kink)', 'US-Tank (Kink)', 'LS-Tank (Kink)')
%     
%     grid minor
%     axis equal
% 
% figure
%     plot(Xt_Upper_End(:,1) , Xt_Upper_End(:,2))
%     hold on
%     plot(Xt_Lower_End(:,1) , Xt_Lower_End(:,2))
%     plot(Xt_Upper_End(:,1) , Y_Upper_End, '--', 'LineWidth', 1.5)
%     plot(Xt_Lower_End(:,1) , Y_Lower_End, '--', 'LineWidth', 1.5)          
%     legend('US (Tip)', 'LS (Tip)', 'US-Tank (Tip)', 'LS-Tank (Tip)')
% 
%     grid minor
%     axis equal

%% Area calculation
% Nodes over the airfoil
x_root  = chords(fuel_tank_start) .* x;
x_end   = chords(fuel_tank_end)   .* x;
x_kink  = chords(kink_index)      .* x;

Area_Root = trapz(x_root, Y_Upper_Root - Y_Lower_Root);          
Area_Kink = trapz(x_kink, Y_Upper_Kink - Y_Lower_Kink);
Area_End  = trapz(x_end,  Y_Upper_End  - Y_Lower_End);

% Fuel volume
Tank_Volume = ((Area_Root + Area_Kink + sqrt(Area_Root * Area_Kink)) *                                      b_k + ...
              ((Area_End  + Area_Kink + sqrt(Area_Kink * Area_End)   * (position(fuel_tank_end) * b / 2.0 - b_k) ))) / 3.0;
Tank_Volume = Tank_Volume * 2.0; % [m3]


end


