function Approximation_Error = Error_Function( DesignVec , x_upper, y_upper )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Euclidean distance between airfoil coordinates and CST shape
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018


x = x_upper;

N1 = 0.5;   %Class function N1
N2 = 1;     %Class function N2

zeta_u = 0.000;     %upper surface TE gap

Au = DesignVec;
nu = length(Au)-1;

%evaluate required functions for each X-ordinate
for i = 1:length(x)
    
    %calculate Class Function for x(i):
    C(i) = (x(i)^N1)*(1-x(i))^N2;
    
    %calculate Shape Functions for upper and lower surface at x(i)
    Su(i) = 0;  %Shape function initially zero
    for j = 0:nu
        Krnu = factorial(nu)/(factorial(j)*factorial(nu-j));
        Su(i) = Su(i) + Au(j+1)*Krnu*(1-x(i))^(nu-j)*x(i)^(j);
    end
    
    %calculate upper and lower surface ordinates at x(i)
    Yu(i) = C(i)*Su(i) + x(i)*zeta_u;
    
end

Yust = Yu';

% Norma eucl�dea
Approximation_Error = sqrt((sum((Yust-y_upper).^2)));

end

