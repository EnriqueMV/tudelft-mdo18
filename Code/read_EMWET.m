function [position, chords, thickness] = read_EMWET

filename = 'test.weight';

%% Initialize variables.
startRow = 3;
endRow = inf;

%% Format string for each line of text:
% column 1-6: double (%f)
formatSpec = '%10f%11f%9f%11f%12f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
textscan(fileID, '%[^\n\r]', startRow(1)-1, 'WhiteSpace', '', 'ReturnOnError', false);
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', '', 'WhiteSpace', '', 'ReturnOnError', false);
for block=2:length(startRow)
    frewind(fileID);
    textscan(fileID, '%[^\n\r]', startRow(block)-1, 'WhiteSpace', '', 'ReturnOnError', false);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', '', 'WhiteSpace', '', 'ReturnOnError', false);
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Allocate imported array to column variable names
position_EMWET = dataArray{:, 1};
chord_EMWET    = dataArray{:, 2};
% 
% upper_thickness_EMWET = dataArray{:, 3};
% lower_thickness_EMWET = dataArray{:, 4};
% left_thickness_EMWET  = dataArray{:, 5};
% right_thickness_EMWET = dataArray{:, 6};

%% Interpolation to desired spanwise position
% vq = interp1(x,v,xq) returns interpolated values of a 1-D function at 
% specific query points using linear interpolation. 
% Vector x contains the sample points, and v contains the corresponding values, v(x). 
% Vector xq contains the coordinates of the query points.
% If you have multiple sets of data that are sampled at the same point coordinates, 
% then you can pass v as an array. Each column of array v contains 
% a different set of 1-D sample values.

sampled_data = [dataArray{:, 2} dataArray{:, 3} dataArray{:, 4} dataArray{:, 5} dataArray{:, 6}];
position     = 0:0.01:1;

interpolated_data = interp1(position_EMWET, sampled_data, position, 'linear', 'extrap');

chords          = interpolated_data(:,1);
thickness.right = interpolated_data(:,2) / 1000;
thickness.left  = interpolated_data(:,3) / 1000;
thickness.upper = interpolated_data(:,4) / 1000;
thickness.lower = interpolated_data(:,5) / 1000;



