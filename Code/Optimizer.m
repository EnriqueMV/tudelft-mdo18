%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          Optimizer
% 
%    Defines optimization initial guess, parameters, and bounds.
%
%    Tools: Wing_Optimization
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

%% Clear workspace and output screen
clear
close all
clc

disp('*************************')
disp('Wing Optimization Problem')
disp('*************************')

t_whole_problem = tic;

fprintf('Enrique Millan Valbuena')
fprintf('\n463 426 8')
fprintf('\nMDO 2017/18')

addpath(genpath('./EMWET'));
addpath(genpath('./Q3DSolver_Inviscid'));
addpath(genpath('./Q3DSolver_Viscid')  );

%% Start parallel computation
%parpool

%% Load initial design

load('Initial_Design.mat', 'Design_Point');

global Design_Point

%% MDA iteration properties

Design_Point.MDA.Max_Iterations = 15;
Design_Point.tolerances.MDA.E   = 10.0^(-3.0);
Design_Point.tolerances.MDA.FW  = 10.0^(-3.0);
Design_Point.tolerances.MDA.Ww  = 10.0^(-3.0);

%% Extract data for bounds

b           = Design_Point.Geometry.b;
Chords      = Design_Point.Geometry.Chords;
Taper_Ratio = Design_Point.Geometry.Taper_Ratio;
Sweep       = Design_Point.Geometry.Sweep;
Twist       = Design_Point.Geometry.Twist;

%% Design vector - initial point
% Design vector
%                           _____ _____ ________________
%x = [b Chord_r Taper_ratio Sweep Twist CST_Coefficients];

%x0     = [             b   Chord root Taper ratio Sweep Twist CST]
x0_ones = 1.0 * ones(1, 1 + 1        + 1       +   2   + 3   + Design_Point.Geometry.N_CST);

%% Optimizer options
% Lower and upper bounds
%x                        = [b            Chord root        Taper ratio         Sweep                           Twist
Fuel_Opt.lb               = [ 42.0 / b     8.0 / Chords(1)  0.1 / Taper_Ratio   25.0 / Sweep(1) 25.0 / Sweep(2) abs(0.1 / Twist(1)) abs(0.1 / Twist(2)) abs(0.1 / Twist(3)), - 1.5 .* ones(1, Design_Point.Geometry.N_CST)];
Fuel_Opt.ub               = [ 55.0 / b    14.0 / Chords(1)  0.5 / Taper_Ratio   40.0 / Sweep(1) 40.0 / Sweep(2) abs(5.0 / Twist(1)) abs(5.0 / Twist(2)) abs(5.0 / Twist(3)),   1.5 .* ones(1, Design_Point.Geometry.N_CST)];  

% Output functions
Fuel_Opt.options.PlotFcns = {@optimplotx @optimplotfval @optimplotconstrviolation...
                             @optimplotstepsize @optimplotfunccount @optimplotfirstorderopt};

% Display and algorithm
Fuel_Opt.options.Display             = 'iter-detailed';
Fuel_Opt.options.Algorithm           = 'sqp';
Fuel_Opt.options.Diagnostics         = 'on';
Fuel_Opt.options.Use_Parallel        = 'on';

% Optimization tolerances
Fuel_Opt.options.FunctionTolerance   = 10.0^(-3.0);
Fuel_Opt.options.OptimalityTolerance = 10.0^(-3.0);
Fuel_Opt.options.MaxIterations       = 100;
Fuel_Opt.options.DiffMinChange       = 10.0^(-2.0);
Fuel_Opt.options.DiffMaxChange       = 10.0^(-1.0);

format short e

fprintf('\n Checking the starting point is inside the limits:')
fprintf('\n Lower bound - Starting point - Upper bound - Boolean check')

for i = 1:length(x0_ones)
    fprintf('\n %f / %f / %f / %f', Fuel_Opt.lb(i), x0_ones(i), Fuel_Opt.ub(i), (Fuel_Opt.lb(i) < x0_ones(i)) * (x0_ones(i) < Fuel_Opt.ub(i)))
end

%% Optimizer run
t_start_optimizer = tic;  

    [x,FVAL,EXITFLAG,OUTPUT,LAMBDA,GRAD,HESSIAN, History, searchdir] = Wing_Optimization(x0_ones, Fuel_Opt);
    

%% Update Design_Point to optimal design vector
% Additional "Driver" call to update the global variable Design_Point
Driver(x);

%% Computing total times
t_elapsed_optimizer = toc(t_start_optimizer);

t_elapsed_whole_problem = toc(t_whole_problem);

save 'Optimization_Problem_Results'
