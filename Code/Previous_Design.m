function Design_Point = Previous_Design
%% Previous design information calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Computes the Design_Point for the initial aircraft
%
% Tool(s): EMWET, Q3D_Solver, Breguet
% 
% User inputs concerning the initial design should be done here.
%
% Generates Initial_Design.mat with Design_Point structure.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

t_start_previous_design = tic;

addpath(genpath('./EMWET'));
addpath(genpath('./Q3DSolver_Inviscid'));
addpath(genpath('./Q3DSolver_Viscid')  );

disp('************************************')
disp('Initial design variables computation')
disp('************************************')

%% Tolerances
Design_Point.tolerances.MDA.FW = 0.001; % [relative]
Design_Point.tolerances.MDA.E  = 0.001; % [relative]
Design_Point.tolerances.MDA.Ww = 0.001; % [relative]

%% Iterations
Design_Point.MDA.Max_Iterations = 10;

%% Span and kink position
b   = 47.57; % b [m]
b_k = 5.5;   % b_k [m], comes from the Boeing document. 

%% Angles in the original wing
Delta_Kink = 1;                    % [deg]
Twist      = [-1.0  ; -2.0 ; -4.0]; % [deg]
Sweep      = [31.5 ;  31.5 ];      % [deg]

%% Chords

Taper_Ratio = 0.21;

Chords(1) = 11.0; % [m] From Boeing document
Chords(2) = Chords(1) + b_k * (tand(Delta_Kink) - tand(Sweep(1)));
Chords(3) = Chords(1) * Taper_Ratio;

%% Weights

Design_Point.Conditions.Design = 0;

Design_Point.MTOW  = 135071.00; % [kg]
Design_Point.FW    =  25735.00; % [kg]
Design_Point.MFPLW =  50304.00; % [kg]
Design_Point.MZFW  = 126099.00; % [kg]
Design_Point.OEW   =  84541.00; % [kg]
Design_Point.DPL   =  24795.00; % [kg]

Design_Point.Previous_Design.MTOW = 135071.00; % [kg]
Design_Point.Previous_Design.FW   =  25735.00; % [kg]

Design_Point.n = 2.5;  % []
Design_Point.g = 9.81; % [m/s2]

%% CST Coefficients
% Select here the number of Bernstein coefficients per surface

[CST_Coefficients(1,:), t_c_max] = Bernstein_Airfoils(6);

% Thickness to chord ratio per airfoil
Design_Point.Geometry.Root.t_c = 10.0;
Design_Point.Geometry.Kink.t_c = 9.0;
Design_Point.Geometry.Tip.t_c  = 8.0;

% Airfoils are fitted to make them less thinner in the tip direction
CST_Coefficients(1,:) = CST_Coefficients(1,:) .* (Design_Point.Geometry.Root.t_c / t_c_max);
CST_Coefficients(2,:) = CST_Coefficients(1,:) .* (Design_Point.Geometry.Kink.t_c / t_c_max);
CST_Coefficients(3,:) = CST_Coefficients(1,:) .* (Design_Point.Geometry.Tip.t_c  / t_c_max);

%% Prepare data structures

% 1) Wing geometry
% 2) Flight conditions
% 3) Engine
% 4) Airfoil positions
% 5) Spar positions
% 6) Fuel tank
%      ||
%      ||
%      ||
%     \  /
%      \/
%% Wing geometry
Design_Point.Geometry.b           = b;     % [m]
Design_Point.Geometry.b_k         = b_k;   % [m]
Design_Point.Geometry.Chords      = Chords;      % [m]
Design_Point.Geometry.Taper_Ratio = Taper_Ratio; % []

Design_Point.Geometry.Sweep       = Sweep;      % [deg]
Design_Point.Geometry.Twist       = Twist;      % [deg]
Design_Point.Geometry.Sweep_Kink  = Delta_Kink; % [deg]
Design_Point.Geometry.Dihedral    = 0.0;        % [deg]

Design_Point.Geometry.CST         = CST_Coefficients;         % []
Design_Point.Geometry.N_CST       = numel(CST_Coefficients);  % []

[Design_Point.Geometry.S2, ...                             % [m2]
 Design_Point.Geometry.MAC] = Wing_Area_MAC(Design_Point); % [m]

Design_Point.Geometry.S = 2.0 * Design_Point.Geometry.S2;  % [m2]

Design_Point.Geometry.AR0 = 8.0;

% Store original values to ease future comparison
Design_Point.Previous_Design.Geometry = Design_Point.Geometry;

%% Flight conditions
Design_Point.Flight.V_cruise   = 236.6;                 % [m/s]
Design_Point.Flight.V_critical = 251.5;                 % [m/s]
Design_Point.Flight.M_MO       = 0.86;                  % []
Design_Point.Flight.h_cruise   = 11887.2;               % [m]
Design_Point.Flight.CT         = 94.2141 * 10.0^(-6.0); % [1/s]
Design_Point.Flight.R          = 11065 * 1000;           % [m]

% ISA conditions
[Design_Point.Flight.T,   Design_Point.Flight.p, ...
 Design_Point.Flight.rho, Design_Point.Flight.mu, ...
 Design_Point.Flight.a                               ]...
                  = ISA(Design_Point.Flight.h_cruise);


Design_Point.Flight.M_cruise = Design_Point.Flight.V_cruise / Design_Point.Flight.a;
Design_Point.Flight.M_cutoff = 0.9;
Design_Point.Flight.M_stable = 0.8;

Design_Point.Flight.Re       = Design_Point.Geometry.MAC * Design_Point.Flight.V_cruise ...
                             * Design_Point.Flight.rho / Design_Point.Flight.mu;
                         
Design_Point.Flight.q          = 0.5 * Design_Point.Flight.rho * Design_Point.Flight.V_cruise^2.0;                         
Design_Point.Flight.q_critical = 0.5 * Design_Point.Flight.rho * Design_Point.Flight.V_critical^2.0;

%% Engine
Design_Point.Engine.Weight   = 4300; % [kg]
Design_Point.Engine.Position = b_k;  % [m]

%% Airfoil positions
[x, y, z] = Airfoil_Positions(Design_Point);

Design_Point.Geometry.Root.x = x(1);
Design_Point.Geometry.Kink.x = x(2);
Design_Point.Geometry.Tip.x  = x(3);

Design_Point.Geometry.Root.y = y(1);
Design_Point.Geometry.Kink.y = y(2);
Design_Point.Geometry.Tip.y  = y(3);

Design_Point.Geometry.Root.z = z(1);
Design_Point.Geometry.Kink.z = z(2);
Design_Point.Geometry.Tip.z  = z(3);

%% Spar positions (% w.r.t. local chord)
Design_Point.Spars.Root.Front = 0.15;
Design_Point.Spars.Root.Rear  = 0.65;

Design_Point.Spars.Kink.Front = 0.15;
Design_Point.Spars.Kink.Rear  = 0.65;

Design_Point.Spars.Tip.Front = 0.15;
Design_Point.Spars.Tip.Rear  = 0.65;

%% Fuel tank
Design_Point.FuelTank.f_tank  = 0.93;   % []
Design_Point.FuelTank.Density = 806.5;  % [kg/m3] 
Design_Point.FuelTank.Start   = 0.01;   % [%] relative to b/2
Design_Point.FuelTank.End     = 0.85;   % [%] relative to b/2

%% Sizing analysis
%tic
cd Q3DSolver_Inviscid
    [Loads, Inviscid_Aerodynamics] = Q3D_Inviscid(Design_Point); % Q3DSolver Inviscid call 
cd ..
%toc

Design_Point.Aerodynamics.Sizing.Loads    = Loads;
Design_Point.Aerodynamics.Sizing.Analysis = Inviscid_Aerodynamics;

%% Structural Analysis
%tic
    [Design_Point.Wing_Weight, Design_Point.FuelTank.Volume] = EMWET_Start(Design_Point, Loads);
%toc

[Design_Point.Structure.y, Design_Point.Structure.chords, ...
 Design_Point.Structure.thickness] = read_EMWET;

Design_Point.M_AW = Design_Point.OEW + Design_Point.DPL - Design_Point.Wing_Weight;

% Save previous design
Design_Point.Previous_Design.FuelTank = Design_Point.FuelTank;

%% Aerodynamic analysis (CD of non-lifiting surfaces)

% Efficiency
Design_Point.Aerodynamics.E = 16;

cd Q3DSolver_Viscid
    %tic
    [Aerodynamic_Analysis] = Q3D_Viscid(Design_Point);
    %toc
cd ..

Design_Point.Aerodynamics.CLwing = Aerodynamic_Analysis.CLwing;
Design_Point.Aerodynamics.CDwing = Aerodynamic_Analysis.CDwing;
Design_Point.Aerodynamics.CD_AW0 = Aerodynamic_Analysis.CLwing / Design_Point.Aerodynamics.E - Aerodynamic_Analysis.CDwing;
Design_Point.Aerodynamics.Cruise_Analysis = Aerodynamic_Analysis;

%fprintf('CD_AW0: %f \n', Design_Point.Aerodynamics.CD_AW0)

%disp('Done.')
% Include previous results
% Wing loading at MTOW
Design_Point.Previous_Design.Aerodynamics.WS_MTOW = 2192.58;
Design_Point.Previous_Design.Aerodynamics = Design_Point.Aerodynamics;

%% Planform plot
% TODO: Update plotting function with new Design_Point structure
% cd Figures/Initial_Design
% planform_view(AC_Inviscid, 'Initial planform view', 'planform_0')
% 
% cd ..
% cd ..

%% Fuel check
% TODO: update code with new Design_Point structure
% disp('-----------------------------')
% disp('Executing fuel capacity check')
% disp('-----------------------------')
% V_fuel = Design_Point.FW / Design_Point.FuelTank.Density * 1000.0; % [l]
% Fuel_Tank_Volume
% volume_check = V_fuel / Tank_Volume / Design_Point.FuelTank.f_tank;
% disp('Volume of the tank used by the fuel is (%)')
% disp(volume_check*100)

%% End of initial design
disp('***********************************')
disp('Initial design succesfully computed')
disp('***********************************')
 t_elapsed_previous_design = toc(t_start_previous_design);
 disp('Elapsed time is:')
 disp(t_elapsed_previous_design)
 
 Design_Point.Conditions.Design = 1;
 
 save Initial_Design.mat Design_Point
 
end