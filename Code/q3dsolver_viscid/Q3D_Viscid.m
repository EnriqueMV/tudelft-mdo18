function [Aerodynamic_Forces] = Q3D_Viscid(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                      AERODYNAMIC ANALYSIS
% 
% Computes the wing loads taking into account skin friction.
% Allows for the computation of dimensionless drag coeff.
%
% Tool: Q3D_Solver Viscid mode
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

%disp('-----------------------------------------')
%disp('Aerodynamics analysis (Q3D_Solver Viscid)')
%disp('-----------------------------------------')

%% Prepare data

% Angles
Delta_Kink = Design_Point.Geometry.Sweep_Kink; % [deg]

% Weights
if Design_Point.Conditions.Design == 0
    
    MTOW = Design_Point.MTOW;
    %fprintf('\n Loads MTOW: %f \n', MTOW)
    
else
    
   MTOW = Design_Point.M_AW + Design_Point.Wing_Weight + Design_Point.FW;
    %fprintf('\n Loads MTOW: %f \n', MTOW)
    
end

FW   = Design_Point.FW;

% Length
b   = Design_Point.Geometry.b;
b_k = Design_Point.Geometry.b_k;

% Sweep angle
Sweep_Root = Design_Point.Geometry.Sweep(1); % [deg]

% Twist angle
Twist_Root = Design_Point.Geometry.Twist(1); % [deg]
Twist_Kink = Design_Point.Geometry.Twist(2); % [deg]
Twist_Tip  = Design_Point.Geometry.Twist(3); % [deg]

% Chords
Chord_Root = Design_Point.Geometry.Chords(1);
Chord_Kink = Chord_Root + b_k * (tand(Delta_Kink) - tand(Sweep_Root));
Chord_Tip  = Design_Point.Geometry.Chords(3);

%% Geometry
%Semi span
AC.Geom.span = b / 2.0;

% Wing incidence angle (degree)
AC.Wing.inc  = 0;

% Airfoil positions
x_root = Design_Point.Geometry.Root.x;  % Root leading edge x-position
y_root = Design_Point.Geometry.Root.y;  % Root leading edge y-position
z_root = Design_Point.Geometry.Root.z;  % Root leading edge z-position

x_kink = Design_Point.Geometry.Kink.x;  % Kink leading edge x-position
y_kink = Design_Point.Geometry.Kink.y;  % Kink leading edge y-position
z_kink = Design_Point.Geometry.Kink.z;  % Kink leading edge z-position

x_tip = Design_Point.Geometry.Tip.x; % Tip leading edge x-position
y_tip = Design_Point.Geometry.Tip.y; % Tip leading edge y-position                                                    
z_tip = Design_Point.Geometry.Tip.z; % Tip leading edge z-position                                

%                [m]     [m]   [m]      [m]       [deg]
AC.Wing.Geom = [x_root y_root z_root Chord_Root Twist_Root;
                x_kink y_kink z_kink Chord_Kink Twist_Kink;
                x_tip  y_tip  z_tip  Chord_Tip  Twist_Tip ];       
            
% Airfoil coefficients input matrix
AC.Wing.Airfoils = Design_Point.Geometry.CST;
            
%% Flight conditions
AC.Wing.eta = [0; b_k / (b / 2.0) ;1];  % Spanwise location of the airfoil sections

AC.Visc = 1;

%% Flight Condition

AC.Aero.V   = Design_Point.Flight.V_cruise; % Flight speed (m/s)
AC.Aero.alt = Design_Point.Flight.h_cruise;        % Flight altitude (m)

%% ISA

AC.Aero.rho = Design_Point.Flight.rho; % Air density [kg/m3]

%% Flow dimensionless numbers
AC.Aero.Re  = Design_Point.Flight.Re;                   % Reynolds number (based on mean aerodynamic chord)
AC.Aero.M   = Design_Point.Flight.M_cruise;             % Flight Mach number
AC.Aero.CL  = Design_Point.g * sqrt(MTOW*(MTOW - FW)) / Design_Point.Flight.q / Design_Point.Geometry.S; % Lift coefficient

%fprintf('\n (Cruise) CL: %f \n', AC.Aero.CL)

% To prevent the code from crashing
 if AC.Aero.M > Design_Point.Flight.M_cutoff
     AC.Aero.M = Design_Point.Flight.M_stable; 
     AC.Aero.V = AC.Aero.M * Design_Point.Flight.a; 
 end

%tic
Aerodynamic_Forces = Q3D_solver(AC);
%toc
%disp('Time required by Q3DSolver inviscid')
end
