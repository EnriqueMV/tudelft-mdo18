function [x, y, z] = Airfoil_Positions(Design_Point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                Leading edge airfoil positions
%
%   Note: it has not been generalized for the number of airfoils.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enrique Millan Valbuena
% 463 426 8
% May 2018

Dihedral = Design_Point.Geometry.Dihedral;

b   = Design_Point.Geometry.b;
b_k = Design_Point.Geometry.b_k;

Sweep_Root = Design_Point.Geometry.Sweep(1);
Sweep_Kink = Design_Point.Geometry.Sweep(2);

Chord_Kink = Design_Point.Geometry.Chords(2);
Chord_Tip  = Design_Point.Geometry.Chords(3);

% Airfoil positions
x(1) = 0.0;                           % Root leading edge x-position
y(1) = 0.0;                           % Root leading edge y-position
z(1) = 0.0;                           % Root leading edge z-position

x(2) = b_k * tand(Sweep_Root);      % Kink leading edge x-position
y(2) = b_k;                         % Kink leading edge y-position
z(2) = Chord_Kink * tand(Dihedral); % Kink leading edge z-position

x(3) = b_k * tand(Sweep_Root) + (b / 2 - b_k) * tand(Sweep_Kink); % Tip leading edge x-position
y(3) = b / 2;                                                     % Tip leading edge y-position
z(3) = Chord_Tip * tand(Dihedral);                                % Tip leading edge z-position