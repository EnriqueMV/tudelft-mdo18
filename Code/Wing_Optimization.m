function [x,FVAL,EXITFLAG,OUTPUT,LAMBDA,GRAD,HESSIAN, history, searchdir] = Wing_Optimization(x0, optimizer_data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                     Optimizer driver
% 
% This function avoids going twice through the MDA loop to 
% evaluate the objective function and the constraints.
%
% Saves history for certain optimization parameters such as:
%   - Design vector, 
%   - Function evaluations 
%   - First order optimality
%   - Step size
%
%  ToDo: Add the possibility of storing in history the constraints values.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1 % No options supplied
    optimizer_data = [];
end

xLast = []; % Last place computeall was called
myf   = []; % Use for objective at xLast
myc   = []; % Use for nonlinear inequality constraint
myceq = []; % Use for nonlinear equality constraint

% Set up shared variables with OUTFUN
history.x             = [];
history.fval          = [];
history.firstorderopt = [];
history.funccount     = [];
history.stepsize      = [];
history.lssteplength  = [];

searchdir = [];

optimizer_data.options.OutputFcn = {@outfun};

fun  = @Objective_Function; % the objective function, nested below
cfun = @Constraints;        % the constraint function, nested below

cont_f = 1;
cont_c = 1;

t_funceval = 0.0;
t_consteval = 0.0;

% Call fmincon
[x,FVAL,EXITFLAG,OUTPUT,LAMBDA,GRAD,HESSIAN] = fmincon(fun,...
                                                       x0, [],[],[],[],    ...
                                                       optimizer_data.lb,  ... 
                                                       optimizer_data.ub,  ...
                                                       cfun,               ...
                                                       optimizer_data.options);
                                                   
save times_optimization t_funceval t_consteval

    function y = Objective_Function(x)
        func_eval_start = tic;
        fprintf('\n \n Objective function evaluation')
        
        if ~isequal(x, xLast) % Check if computation is necessary
            fprintf('\n Driver call')
            [myf,myc,myceq] = Driver(x);
            xLast = x;
        end
        
        % Now compute objective function
        y = myf;
        fprintf('\n (Wing_Optimization) f(x) = %f', y)
        t_funceval(cont_f) = toc(func_eval_start);
        cont_f = cont_f + 1;
    end

    function [c,ceq] = Constraints(x)
        const_eval_start = tic;
        fprintf('\n \n Constraints evaluation')
        
        if ~isequal(x,xLast) % Check if computation is necessary
            fprintf('\n Driver call')
            [myf,myc,myceq] = Driver(x);
            xLast = x;
        end
        
        % Now compute constraint functions
        c   = myc; % In this case, the computation is trivial
        ceq = myceq;
        fprintf('\n (Wing_Optimization) c(x) <= %f %f %f', myc(1), myc(2), myc(3))
        t_consteval(cont_c) = toc(const_eval_start);
    end

    function stop = outfun(x,optimValues,state)
    stop = false;

       switch state
           case 'init'
               
           case 'iter'
               % Concatenate current point and objective function
               % value with history. x must be a row vector.
               
               % Make sure 
               x = reshape(x, [1, length(x)]);
               
               history.fval          = [history.fval;          optimValues.fval         ];
               history.x             = [history.x;                         x            ];
               history.firstorderopt = [history.firstorderopt; optimValues.firstorderopt];
               history.funccount     = [history.funccount;     optimValues.funccount    ];
               history.stepsize      = [history.stepsize;      optimValues.stepsize     ];
               history.lssteplength  = [history.lssteplength;  optimValues.lssteplength ];
               
               % Concatenate current search direction with 
               % searchdir.
               searchdir = [searchdir; ...
                            optimValues.searchdirection'];
           case 'done'
               
           otherwise
       end
    end

end