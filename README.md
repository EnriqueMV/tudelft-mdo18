# Multidisciplinary Optimization Course (TU Delft, 2018)

A multidisciplinary optimization framework has been built and set-up in MatLab as part of the full-fillment of the course. Within this environment we shall minimize the amount of fuel a Boeing 767-300 requires for a design range and payload, considering the design to be in a conceptual phase. Only the wing of the airplane is modeled as a geometrical ﬁgure: the planform is built by joining two trapezoidal elements and the thickness given as an interpolation between three airfoil sections at the root, kink and tip. Four different disciplines take part in the optimization: Loads, Aerodynamic Performance, Structural Analysis and ﬁnally the Breguet Fraction Method for Performance evaluations.

Grade: 10/10.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

OS: Windows. (Unfortunately some of the files are precompiled and will only run on Windows (.EXE))

Software: Matlab

### Installing

Since the code runs on Matlab, you only need to install that package following the software instructions. 

## Running the tests

Before launching the optimizer, open the file Previous_Design.m and read a bit through it. 
It contains the initial design point from which the optimization will take place.

Not all the initial point variables will fit together perfectly. Run this code to perform an MDA iteration to find a feasible solution for the initial data provided. At the same time, this will double check the well-functioning of all the simulations tools. 

## Authors

 **Enrique Millán Valbuena** - *Initial work*

## Acknowledgments


* Quasi-Three-Dimensional Aerodynamic Solver for Multidisciplinary Design Optimization of Lifting Surfaces. Authors: J. Mariens, A. Elham, and M. J. L. van Tooren

* Development and implementation of an advanced, design-sensitive method for wing weight estimation. Authors: A. Elham , G. La Rocca, M.J.L. van Tooren

